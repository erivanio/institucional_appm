from django.conf.urls import patterns, include, url
from django.contrib import admin
from apps.core.views import ImportFileRedirectView

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'apps.core.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'import-file/$', ImportFileRedirectView.as_view(), name='import_file'),
)
