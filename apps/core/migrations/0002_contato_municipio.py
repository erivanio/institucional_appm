# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='contato',
            name='municipio',
            field=models.ForeignKey(blank=True, to='core.Municipio', null=True),
            preserve_default=True,
        ),
    ]
