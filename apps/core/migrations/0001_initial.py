# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contato',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=200, null=True, blank=True)),
                ('endereco', models.CharField(max_length=200, null=True, verbose_name=b'Endere\xc3\xa7o', blank=True)),
                ('profissao', models.CharField(max_length=200, null=True, verbose_name=b'Profiss\xc3\xa3o', blank=True)),
                ('empresa', models.CharField(max_length=200, null=True, blank=True)),
                ('dt_nasc', models.DateField(null=True, verbose_name=b'Data de Nascimento', blank=True)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Grupo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=200)),
                ('usuario', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Municipio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nome', models.CharField(max_length=200)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Telefone',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('descricao', models.CharField(max_length=200, null=True, blank=True)),
                ('telefone', models.CharField(max_length=15)),
                ('contato', models.ForeignKey(to='core.Contato')),
                ('grupo', models.ManyToManyField(to='core.Grupo', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
