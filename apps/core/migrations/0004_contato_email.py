# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_contato_nome_completo'),
    ]

    operations = [
        migrations.AddField(
            model_name='contato',
            name='email',
            field=models.CharField(max_length=200, null=True, verbose_name=b'Email', blank=True),
            preserve_default=True,
        ),
    ]
