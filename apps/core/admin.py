from django.contrib import admin
from django import forms
from apps.core.models import Telefone, Grupo, Contato, Municipio


class GrupoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'telefones')
    exclude = ['usuario']
    search_fields = ['nome']

    def telefones(self, obj):
        list_string = []
        for telefone in obj.telefone_set.all():
            string = '%s(%s)'%(telefone.contato.nome, telefone.telefone)
            list_string.append(string)
        return "%s" % (' , '.join(list_string))

    def save_model(self, request, obj, form, change):
        obj.usuario = request.user
        super(GrupoAdmin, self).save_model(request, obj, form, change)


class TelefoneInline(admin.StackedInline):
    model = Telefone
    extra = 1

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == "grupo":
            kwargs["queryset"] = Grupo.objects.filter(usuario=request.user)
        return super(TelefoneInline, self).formfield_for_manytomany(db_field, request, **kwargs)


class ContatoAdmin(admin.ModelAdmin):
    list_display = ('nome', 'municipio', 'email', 'telefones')
    exclude = ['usuario']
    inlines = [TelefoneInline]
    list_filter = ['municipio']
    search_fields = ['nome', 'grupo', 'email', 'profissao', 'empresa', 'dt_nasc', 'nome_completo']

    def telefones(self, obj):
        return "%s" % (' , '.join(telefone.telefone for telefone in obj.telefone_set.all()))

    def save_model(self, request, obj, form, change):
        obj.usuario = request.user
        super(ContatoAdmin, self).save_model(request, obj, form, change)


admin.site.register(Grupo, GrupoAdmin)
admin.site.register(Contato, ContatoAdmin)
admin.site.register(Municipio)