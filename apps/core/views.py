from django.contrib import messages
from django.shortcuts import redirect
from django.views.generic import RedirectView
from apps.core.models import Contato, Telefone


class ImportFileRedirectView(RedirectView):

    url = '/admin/core/contato'

    def post(self, request, *args, **kwargs):
        arquivo = request.FILES.get('file')
        arquivo_linha = arquivo.readlines()
        for line in arquivo_linha:
            line = line.split(';')
            if line[0] is not '':
                contato = Contato()
                contato.usuario = request.user
                contato.nome = line[0]
                contato.save()
                for index in range(1, len(line)):
                    telefone = Telefone()
                    telefone.telefone = line[index]
                    telefone.contato = contato
                    telefone.save()
        arquivo.close()
        messages.success(request, ('Arquivo importado com sucesso'))
        return self.get(request, *args, **kwargs)


def home(request):
    return redirect('/admin')