# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

class Municipio(models.Model):
    nome = models.CharField(max_length=200)

    def __unicode__(self):
        return self.nome


class Grupo(models.Model):
    nome = models.CharField(max_length=200)
    usuario = models.ForeignKey(User)

    def __unicode__(self):
        return self.nome


class Contato(models.Model):
    nome = models.CharField(max_length=200, null=True, blank=True)
    nome_completo = models.CharField(max_length=200, null=True, blank=True)
    usuario = models.ForeignKey(User)
    email = models.CharField('Email', max_length=200, null=True, blank=True)
    endereco = models.CharField('Endereço', max_length=200, null=True, blank=True)
    profissao = models.CharField('Profissão', max_length=200, null=True, blank=True)
    empresa = models.CharField(max_length=200, null=True, blank=True)
    dt_nasc = models.DateField('Data de Nascimento', null=True, blank=True)
    municipio = models.ForeignKey(Municipio, null=True, blank=True)

    def __unicode__(self):
        return self.nome


class Telefone(models.Model):
    descricao = models.CharField(max_length=200, null=True, blank=True)
    telefone = models.CharField(max_length=15)
    contato = models.ForeignKey(Contato)
    grupo = models.ManyToManyField(Grupo, null=True, blank=True)

    def __unicode__(self):
        return self.telefone
